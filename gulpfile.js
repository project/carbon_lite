const gulp = require("gulp");
const plugins = require("gulp-load-plugins")();
const autoprefixer = require("autoprefixer");
const cssnano = require("cssnano");
const pngquant = require("imagemin-pngquant");
const mozjpeg = require("imagemin-mozjpeg");

// Source and destination path
const paths = {
  styles: {
    src: "src/scss/**/*.scss",
    dest: "build/css"
  },
  scripts: {
    src: "src/js/**/*.js",
    dest: "build/js"
  },
  images: {
    src: "src/img/**/*.{jpg,jpeg,png,svg}",
    dest: "build/img"
  }
};

// Post css ( browser prefixes and css minification )
const postcssProcessors = [autoprefixer(), cssnano()];

// Images processing setting
const imgOptions = [
  mozjpeg({
    quality: "86"
  }),
  pngquant({
    quality: [0.7, 0.8]
  })
];

// SASS -> CSS function
function styles() {
  return gulp
    .src(paths.styles.src)
    .pipe(plugins.changed(paths.styles.dest))
    .pipe(plugins.sass())
    .pipe(plugins.postcss(postcssProcessors))
    .pipe(gulp.dest(paths.styles.dest));
}

// JS function
function scripts() {
  return gulp
    .src(paths.scripts.src)
    .pipe(plugins.changed(paths.scripts.dest))
    .pipe(plugins.uglify())
    .pipe(gulp.dest(paths.scripts.dest));
}

// Img processing
function images() {
  return gulp
    .src(paths.images.src)
    .pipe(plugins.changed(paths.images.dest))
    .pipe(
      plugins.imagemin(imgOptions, {
        verbose: true
      })
    )
    .pipe(gulp.dest(paths.images.dest));
}

// Watch function
function watch() {
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.styles.src, styles);
}

// Build process
const build = gulp.series(gulp.parallel(styles, scripts));
const watcher = gulp.series(gulp.parallel(build, images), watch);

// Standalone tasks
gulp.task("styles", styles);
gulp.task("scripts", scripts);
gulp.task("watch", watch);
gulp.task("images", images);

// Default task is gulp watch
gulp.task("build", build);
gulp.task("default", watcher);
