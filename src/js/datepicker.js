(function ($, Drupal) {

  // default options to be used with the datepicker
  var defaultOptions = {
    dateFormat: "yy-dd-mm",
    firstDay: 1,
    gotoCurrent: true,
    changeMonth: true,
    changeYear: true,
  };

  function getOptions(newOptions) {
    var tempObj = defaultOptions;
    for (var property in newOptions) {
      tempObj[property] = newOptions[property];
    }
    return tempObj;
  }

  Drupal.behaviors.setDefaultDatepicker = {
    attach: function(context, settings) {
      if(('input[type="date"]')) {
        $('input[type="date"]').once()
          .datepicker(getOptions({}))
          .attr('type','text')
          .addClass("datepicker-date");
      }

      $(document).on("click", ".datepicker-date", function(e) {
        $(".ui-datepicker-prev").html('<i class="icon icon-left icon-small"></i>');
        $(".ui-datepicker-next").html('<i class="icon icon-right icon-small"></i>');
        console.log("click");
      });
    }
  };
})(jQuery, Drupal);
