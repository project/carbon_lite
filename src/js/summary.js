(function ($, Drupal) {

  Drupal.behaviors.toggleSummary= {
    attach: function attach(context) {
      var details = $(context).find('details').once('trigger-details');

      if (!details.length) {
        return;
      } else {
        var summary = details.find('summary');

        summary.click(function () {
          var icon = $(this).find('i');

          if (icon.hasClass('icon-down')) {
            icon.removeClass('icon-down');
            icon.addClass('icon-right');
          } else {
            icon.removeClass('icon-right');
            icon.addClass('icon-down');
          }
        });
      }
    }
  };
})(jQuery, Drupal);
