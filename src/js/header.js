(function ($, Drupal) {

  Drupal.behaviors.closeAll = {
    attach: function(context, settings) {
      $(".user-action").once().on("mousedown", function() {
        $('.show-element')
          .not($(this).closest(".show-element")) // not the clicked button's parent
          .not($(this).parent().next(".show-element")) // not the clicked button's neighbor - hamburger + main navigation
          .removeClass('show-element');
        $(".button-hamburger-close").addClass("hide-button");
        $(".button-hamburger").removeClass("hide-button");
      });
    }
  };

  Drupal.behaviors.toggleMobileMenu = {
    attach: function(context, settings) {
      $(".button-hamburger").on("click", function(e) {
        e.stopImmediatePropagation();
        $(".region-navigation").addClass("show-element");
        $(".button-hamburger").addClass("hide-button");
        $(".button-hamburger-close").removeClass("hide-button");
      });

      $(".button-hamburger-close").on("click", function(e) {
        e.stopImmediatePropagation();
        $(".region-navigation").removeClass("show-element");
        $(".button-hamburger-close").addClass("hide-button");
        $(".button-hamburger").removeClass("hide-button");
      });
    }
  };

  Drupal.behaviors.toggleSearchForm = {
    attach: function (context, settings) {
      // open the search form
      $(".button-search").on("click", function(e) {
        e.stopImmediatePropagation();
        $(".search-block-form").addClass("show-element");
      });

      $(".button-search-reset").on("click", function(e) {
        e.stopImmediatePropagation();
        $(".search-block-form").removeClass("show-element");
      });
    }
  };

  Drupal.behaviors.toggleUserAccountDropdown = {
    attach: function (context, settings) {
      // open user account dropdown
      $(".button-account").on("click", function(e) {
        e.stopImmediatePropagation();
        $(".block-account").toggleClass("show-element");
      });
    }
  };
})(jQuery, Drupal);
