## For theme development:
1. in terminal go to the themes folder of your sandbox project:
    1. run in terminal: `git init`
    2. clone the theme repository
2. download `defaults.zip` from Downloads and extract it in the root of your project (not theme)
3. if needed (in case of setup issues)
    1. open `settings.local.php` check and, if needed, change the database connection info (database name, username, password) in the `$databases` array
    2. copy the three files from the `defaults/` folder and paste them in `web/sites/default/`

## Gulp setup

1. Download the .lando.yml file and place it in the root of the project
2. If you have the project already setup run

```bash
lando rebuild
```

3. Go to the theme directory and run

```bash
cd web/themes/carbon_lite
lando npm install
```

## Gulp usage

Build and watch

```bash
lando gulp
```

Only build

```bash
lando gulp build
```


## Icomoon

For adding new icons to the existing icon set:

1. Go to Downloads and download `icomoon.json`
2. Open https://icomoon.io/app/
3. Click Import icons on the top bar on the left-hand side
4. Upload the `icomoon.json` file
5. Click Yes in the popup about loading settings from the selection file
6. The icons that were previously selected will be shown first, under "icomoon", and you can start adding new icons now by selecting them from the other sets on the page
7. The default size of the icons may differ, so it is important that you check the number in line with the set's title (icomoon, Steadysets, Zondicons etc), and if different from our imported icon set size (should be 20):
    1. click the hamburger icon next to the number (of the set of the icon you selected, not imported) 
    2. click Properties
    3. click Reset Grid Size 
    4. enter the size of our set (should be 20) 
    5. Reset. 

8. That will bring all of the icons to the same size. If still different, try clicking the number itself and selecting the size from the dropdown.
9. Click Generate Font in the bottom bar, to the right-hand side
10. Click Preferences in the top bar, to the left-hand side
11. Check both of the IE-related checkboxes and "Use a class" radio button, everything else should be left as is, and close the popup (don't check the SASS checkbox, it will only make your life difficult later on)
12. Change the names of the icons if necessary
13. Click "Download" in the bottom bar, on the right-hand side
14. You will get a `.zip` file:
    1. extract `selection.json` and rename it to `icomoon.json` - that is the new selection file, if you are updating the `carbon_lite` theme it is imperative that you upload the `icomoon.json` file to BitBucket Downloads
    2. extract the rest of the content, open `style.css` and copy the whole part that relates to the `:before` of the icon classes and replace the existing ones in `carbon_lite/src/scss//base/_icons.scss`
    2. extract the icon font files in the `fonts` folder from the `.zip` file to `carbon_lite/assets/icons`
